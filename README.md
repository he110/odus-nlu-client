# Odus NLU Client - Train, predict and parse 

Odus NLU Client allows you to train Odus bots, get predictions from text and recognize intents with few commands.

## Installation

Install the latest version with

```bash
$ composer require odus/nlu-client
```

## Basic Usage

- //TODO

## Documentation

- //TODO

## Third Party Packages

- //TODO

## About

### Requirements

- //TODO

### Author

Ilya S. Zobenko - <ilya@zobenko.ru> - <http://twitter.com/he110_todd>

### License

Odus NLU Client is licensed under the MIT License - see the `LICENSE` file for details