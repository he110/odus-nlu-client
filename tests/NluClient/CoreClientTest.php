<?php
/*
 * This file is part of the Odus Nlu Client package.
 *
 * (c) Ilya S. Zobenko <ilya@zobenko.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Odus\NluClient;


use Odus\BotInterface;
use PHPUnit\Framework\TestCase;

class CoreClientTest extends TestCase
{

    /**
     * @covers \Odus\NluClient\CoreClient::parse()
     */
    public function testParse()
    {
        $bot = $this->getMockBuilder(BotInterface::class)->getMock();

        $client = new CoreClient($bot);

        $helper = $this->getMockBuilder(CoreHelper::class)->getMock();
        $helper->method("request")->willReturn([
            "result" => [
                ["name" => "answer_3983"],
                ["name" => "answer_3984"]
            ],
            "answer_ranking" => [
                ["action" => "answer_3983", "score" => 0.900011003821765701584],
                ["action" => "answer_3984", "score" => 4.6822645561701605e-8]
            ]
        ]);

        $this->assertEquals("answer_3983", $client->parse(100, "Hi! It's just test"));
    }
}
