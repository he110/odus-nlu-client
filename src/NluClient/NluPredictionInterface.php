<?php
/*
 * This file is part of the Odus Nlu Client package.
 *
 * (c) Ilya S. Zobenko <ilya@zobenko.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Odus\NluClient;

/**
 * Interface for predicted answer with it's score (confidence).
 *
 * @author Ilya S. Zobenko <ilya@zobenko.ru>
 */
interface NluPredictionInterface
{
    /**
     * NluPredictionInterface constructor.
     * @param string $result
     * @param float $score
     */
    public function __construct(string $result, float $score);

    /**
     * @return string
     */
    public function getResult():string;

    /**
     * @return float
     */
    public function getScore():float;
}