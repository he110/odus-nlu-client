<?php
/*
 * This file is part of the Odus Nlu Client package.
 *
 * (c) Ilya S. Zobenko <ilya@zobenko.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Odus\NluClient;

use Odus\BotInterface;

/**
 * Main Interface for all types of NLU clients.
 * Allows to train, predict and parse. Takes Odus's bot as main source of data for every method
 *
 * @author Ilya S. Zobenko <ilya@zobenko.ru>
 */
interface NluClientInterface
{
    public function __construct(BotInterface $bot);

    public function parse(int $userId, string $text, string $model = "test"):?string;

    public function train(string $model = "test"):bool;

    public function getPredictions(string $text, string $model = "test"):?array;

    public function getPrediction(string $text, string $model = "test"):?NluPredictionInterface;
}
