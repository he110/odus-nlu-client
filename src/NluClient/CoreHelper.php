<?php
/*
 * This file is part of the Odus Nlu Client package.
 *
 * (c) Ilya S. Zobenko <ilya@zobenko.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Odus\NluClient;


class CoreHelper
{
    /** @var string  */
    private $host;

    /** @var string */
    private $env;

    const CONNECTION_TIMEOUT = 4;
    const REQUEST_TIMEOUT = 10;

    /**
     * CoreHelper constructor.
     * @param string $host
     * @param string $env
     */
    public function __construct(string $host, string $env = "local")
    {
        if (stripos($host, "http://") === false)
            $host = "http://".$host;
        $this->setHost($host);
        $this->setEnv($env);
        return $this;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        if (!$this->host)
            throw new \LogicException("NLU Core host wasn't specified");
        return $this->host;
    }

    /**
     * @param string $host
     */
    public function setHost(string $host)
    {
        $this->host = $host;
    }

    /**
     * @return string
     */
    public function getEnv(): string
    {
        return $this->env ?? "local";
    }

    /**
     * @param string $env
     */
    public function setEnv(string $env): void
    {
        $this->env = $env;
    }

    public function request(string $endpoint, array $data):?array
    {
        if ("/" !== substr($endpoint, 0, 1))
            $endpoint = "/".$endpoint;

        $data["env"] = $this->getEnv();

        $ch = curl_init($this->getHost().$endpoint);

        curl_setopt_array($ch, [
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => json_encode($data),
            CURLOPT_CONNECTTIMEOUT => static::CONNECTION_TIMEOUT,
            CURLOPT_TIMEOUT => static::REQUEST_TIMEOUT,
            CURLOPT_RETURNTRANSFER => true
        ]);

        $response = curl_exec($ch);
        curl_close($ch);

        if ($result = json_decode($response, true))
            return $result;
        return null;
    }
}