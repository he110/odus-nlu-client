<?php
/*
 * This file is part of the Odus Nlu Client package.
 *
 * (c) Ilya S. Zobenko <ilya@zobenko.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Odus\NluClient;

use Odus\BotInterface;
use Odus\NluClient\Exceptions\InvalidCoreAnswerException;

/**
 * Class with methods and tools, which allows to parse and predict any intents from text.
 * It uses Python NLU Core for parsing
 *
 * @author Ilya S. Zobenko <ilya@zobenko.ru>
 */
class CoreClient implements NluClientInterface
{

    private $coreHelper;

    /** @var BotInterface */
    private $bot;

    /**
     * Creates new instance of CoreClient, based on Odus's bot data
     *
     * @param BotInterface $bot
     */
    public function __construct(BotInterface $bot)
    {
        if(!$host = getenv("CORE_HOST"))
            throw new \LogicException("Can't find `CORE_HOST` config");
        $this->coreHelper = new CoreHelper($host);
        $this->bot = $bot;
    }

    /**
     * Allows to get single answer from core.
     * Answer will be an `Intent` or `Answer` name (e.g. answer_52).
     *
     * @param int $userId Chat-user ID, for dialog state recording
     * @param string $text Text from user, which should be parsed with NLU Core
     * @param string $model Model name from NLU Core. It's `test` by default
     * @return null|string
     * @throws InvalidCoreAnswerException
     */
    public function parse(int $userId, string $text, string $model = "test"): ?string
    {
        $data = [
            "q" => $text,
            "uid" => $userId,
            "model" => $model,
            "bot" => $this->bot->getId()
        ];

        if(!$response = $this->coreHelper->request("parse", $data))
            throw new InvalidCoreAnswerException("Invalid response from NLU Core");

        if (!array_key_exists("result", $response))
            throw new InvalidCoreAnswerException("Can't parse `result` value");

        if (empty($response["result"]))
            return null;

        list($result) = $response["result"][0];
        return $result;
    }

    /**
     * Trains core with Odus's bot data
     * This method required to parse and predict any texts
     *
     * @param string $model Model name from NLU Core. It's `test` by default
     * @return bool
     */
    public function train(string $model = "test"): bool
    {
        // TODO: Implement train() method.
    }

    /**
     * Allows to get an array with multiple an extended answers from core
     * It'll include text result (e.g. answer_52) and it's score (confidence)
     *
     * @param string $text Text from user, which should be parsed with NLU Core
     * @param string $model Model name from NLU Core. It's `test` by default
     * @return array|null
     */
    public function getPredictions(string $text, string $model = "test"): ?array
    {
        // TODO: Implement getPredictions() method.
    }

    /**
     * Allows to get an extended answer from core
     * It'll include text result (e.g. answer_52) and it's score (confidence)
     *
     * @param string $text Text from user, which should be parsed with NLU Core
     * @param string $model Model name from NLU Core. It's `test` by default
     * @return null|NluPredictionInterface
     */
    public function getPrediction(string $text, string $model = "test"): ?NluPredictionInterface
    {
        // TODO: Implement getPrediction() method.
    }

    public function setCoreHelper(CoreHelper $helper) {
        $this->coreHelper = $helper;
    }
}