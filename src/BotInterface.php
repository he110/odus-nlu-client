<?php
/*
 * This file is part of the Odus Nlu Client package.
 *
 * (c) Ilya S. Zobenko <ilya@zobenko.ru>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Odus;


interface BotInterface
{
    public function getId():int;

    public function getName():string;
}